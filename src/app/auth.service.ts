import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { auth } from './firebase';
import { USERS } from './config';

@Injectable()
export class AuthService {

  authState = false;

  constructor(private _http: HttpClient) {
    auth.onAuthStateChanged(authUser => {
      if (authUser) {
        this.authState = true
        this.getAndSaveDetail(authUser)
      } else {
        this.authState = false
      }
    })
  }

  login(data) {
    return auth.signInWithEmailAndPassword(data.email, data.password);
  }

  register(data) {
    return auth.createUserWithEmailAndPassword(data.email, data.password);
  }

  registerInServer(user) {
    return this._http.post(USERS, user);
  }

  logout() {
    auth.signOut();
  }

  getAndSaveDetail(authUser) {
    this._http.post(USERS + '/' + authUser.uid, {})
      .subscribe(res => {
        localStorage.setItem('user', JSON.stringify(res));
      });
  }

}
