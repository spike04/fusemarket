import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { PRODUCTS, CATEGORY } from "./config";

@Injectable()
export class CoreService {
  constructor(private _http: HttpClient) { }

  getProducts() {
    return this._http.get(PRODUCTS);
  }

  getProduct(productId) {
    return this._http.get(PRODUCTS + "/" + productId);
  }

  editProduct(productId, formData) {
    return this._http.post(PRODUCTS + "/" + productId, formData);
  }

  deleteProduct(product) {
    return this._http.delete(PRODUCTS + "/" + product._id);
  }

  getProductsOfUser() {
    return this._http.get(
      PRODUCTS + "/user/" + JSON.parse(localStorage.getItem("user")).user._id
    );
  }

  getProductsOfCategory(categoryId) {
    return this._http.get(PRODUCTS + "/category/" + categoryId);
  }

  getProductsFromSearchParams(text) {
    return this._http.get(PRODUCTS + "/search?s=" + text);
  }

  getCategory() {
    return this._http.get(CATEGORY);
  }

  getSingleCategory(categoryId) {
    return this._http.get(CATEGORY + "/" + categoryId);
  }

  updateCategory(category) {
    return this._http.put(CATEGORY + "/" + category._id, {
      name: category.name
    });
  }

  deleteCategory(category) {
    return this._http.delete(CATEGORY + "/" + category._id);
  }

  addCategory(name) {
    return this._http.post(CATEGORY, { name });
  }

  addProduct(formData) {
    return this._http.post(PRODUCTS, formData);
  }
}
