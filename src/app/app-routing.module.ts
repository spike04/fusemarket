import { SearchProductComponent } from './home/search-product/search-product.component';
import { CategoryProductComponent } from './home/category-product/category-product.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AddProductComponent } from './dashboard/pages/add-product/add-product.component';
import { AllCategoriesComponent } from './dashboard/pages/all-categories/all-categories.component';
import { DashAllProductsComponent } from './dashboard/pages/dash-all-products/dash-all-products.component';
import { DashSummaryComponent } from './dashboard/pages/dash-summary/dash-summary.component';
import { AllProductsComponent } from './home/all-products/all-products.component';
import { FeaturedProductsComponent } from './home/featured-products/featured-products.component';
import { MainComponent } from './home/main/main.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProductDetailComponent } from './home/product-detail/product-detail.component';
import { EditProductComponent } from './dashboard/pages/edit-product/edit-product.component';

const routes: Routes = [
  {
    path: '', component: MainComponent, children: [
      { path: '', component: FeaturedProductsComponent },
      { path: 'products', component: AllProductsComponent },
      { path: 'products/:id', component: ProductDetailComponent },
      { path: 'category/:id', component: CategoryProductComponent },
      { path: 'search', component: SearchProductComponent }
    ]
  },
  {
    path: 'dashboard', component: DashboardComponent, children: [
      { path: '', component: DashSummaryComponent },
      { path: 'product', component: DashAllProductsComponent },
      { path: 'product/add', component: AddProductComponent },
      { path: 'product/:id/edit', component: EditProductComponent },
      { path: 'category', component: AllCategoriesComponent },
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
