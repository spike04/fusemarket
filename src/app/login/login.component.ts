import swal from 'sweetalert2';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6)
    ])
  })

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  loading = false;
  disabled = true;

  constructor(private service: AuthService, private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.loading = true;
    this.service.login(this.form.value)
      .then(authUser => {
        if (authUser) {
          this.loading = false;
          this.router.navigate(['/']);
        } else {
          this.loading = false;
        }
      })
      .catch(err => {
        this.form.reset();
        swal('Error', err.message, 'error');
      })
  }
}
