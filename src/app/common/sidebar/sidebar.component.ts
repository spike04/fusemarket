import { CoreService } from './../../core.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  categories;

  constructor(private service: CoreService) { }

  ngOnInit() {
    this.service.getCategory()
      .subscribe(categories => this.categories = categories);
  }

}
