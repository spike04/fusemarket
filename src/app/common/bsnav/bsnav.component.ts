import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from './../../auth.service';
import { Component, OnInit, Input } from '@angular/core';
import * as firebase from '../../firebase';
import { Router } from '@angular/router';

@Component({
  selector: 'bsnav',
  templateUrl: './bsnav.component.html',
  styleUrls: ['./bsnav.component.css']
})
export class BsnavComponent implements OnInit {

  title = 'FuseMarket';

  form = new FormGroup({
    search: new FormControl('', [
      Validators.required
    ])
  });

  constructor(public authService: AuthService, private router: Router) {
  }

  ngOnInit() {
  }

  searchProduct() {
    if (this.form.value.search.trim().length !== 0) {
      this.router.navigate(['/search'], { queryParams: { s: this.form.value.search } });
    }
  }

}
