import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsnavComponent } from './bsnav.component';

describe('BsnavComponent', () => {
  let component: BsnavComponent;
  let fixture: ComponentFixture<BsnavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BsnavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BsnavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
