import { FormControl, Validators } from '@angular/forms';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6)
    ])
  })

  loading = false;
  disabled = true;

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  constructor(private service: AuthService, private router: Router) { }

  ngOnInit() {
  }

  register() {
    this.loading = true;
    this.service.register(this.form.value)
      .then(authUser => {
        if (authUser) {

          const user = {
            email: authUser.email,
            uuid: authUser.uid
          }

          this.service.registerInServer(user)
            .subscribe((res: any) => {
              localStorage.setItem('user', JSON.stringify(res.user));
              this.loading = false;
              this.router.navigate(['/login']);
              swal('Great', 'Registered Successfully', 'success');
            });
        } else {
          this.loading = false;
        }
      })
      .catch(err => {
        this.form.reset();
        swal('Error', err.message, 'error');
      })
  }
}
