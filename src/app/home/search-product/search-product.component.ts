import { CoreService } from './../../core.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'search-product',
  templateUrl: './search-product.component.html',
  styleUrls: ['./search-product.component.css']
})
export class SearchProductComponent implements OnInit {

  products;
  search = '';

  constructor(private activatedRoute: ActivatedRoute, private service: CoreService) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(value => {
      this.search = value.s;
      this.products = null;
      this.service.getProductsFromSearchParams(value.s)
        .subscribe((products: any) => {
          if (products.length > 0) {
            this.products = products;
          }
        });
    })
  }

}
