import { PRODUCTS } from "./../../config";
import { CoreService } from "./../../core.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "featured-products",
  templateUrl: "./featured-products.component.html",
  styleUrls: ["./featured-products.component.css"]
})
export class FeaturedProductsComponent implements OnInit {
  products;

  constructor(private service: CoreService) {}

  ngOnInit() {
    this.service
      .getProducts()
      .subscribe((products: any) => (this.products = products.slice(0, 8)));
  }
}
