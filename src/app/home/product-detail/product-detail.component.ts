import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CoreService } from '../../core.service';

@Component({
  selector: 'product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  product;

  constructor(private activatedRoute: ActivatedRoute, private service: CoreService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(value => {
      this.service.getProduct(value.id)
        .subscribe(product => this.product = product);
    })
  }
}
