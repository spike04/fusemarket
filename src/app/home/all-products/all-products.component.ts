import { CoreService } from './../../core.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.css']
})
export class AllProductsComponent implements OnInit {

  products;

  constructor(private service: CoreService) { }

  ngOnInit() {
    this.service.getProducts()
      .subscribe(products => this.products = products);
  }

}
