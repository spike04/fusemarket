import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CoreService } from './../../core.service';

@Component({
  selector: 'category-product',
  templateUrl: './category-product.component.html',
  styleUrls: ['./category-product.component.css']
})
export class CategoryProductComponent implements OnInit {

  products;
  name;

  constructor(private service: CoreService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(value => {
      this.products = null;
      this.service.getProductsOfCategory(value.id)
        .subscribe((products: any) => {
          if (products.length > 0) {
            this.products = products
          }
        });
      this.service.getSingleCategory(value.id)
        .subscribe((category: any) => this.name = category.name);
    });
  }

}
