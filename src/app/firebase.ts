import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyDrVqup83KwkDnts7rCtK_OOCJ6e3Q4x70",
    authDomain: "fuseprojects-763cd.firebaseapp.com",
    databaseURL: "https://fuseprojects-763cd.firebaseio.com",
    projectId: "fuseprojects-763cd",
    storageBucket: "fuseprojects-763cd.appspot.com",
    messagingSenderId: "716271467359"
};

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

const auth = firebase.auth();

export {
    auth
};