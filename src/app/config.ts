export const BASE = 'https://katiparxa.com:5500/';
// export const BASE = 'https://localhost:5500/';
export const PRODUCTS = BASE + 'products';
export const USERS = BASE + 'users';
export const CATEGORY = BASE + 'categories';

