import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormGroup, Validators, FormControl } from "@angular/forms";

@Component({
  selector: "dash-categories",
  templateUrl: "./dash-categories.component.html",
  styleUrls: ["./dash-categories.component.css"]
})
export class DashCategoriesComponent implements OnInit {
  @Input() categories;

  @Output() deleteCategory = new EventEmitter();
  @Output() editCategory = new EventEmitter();

  categoryToEdit;

  modalRef: any;

  admin: String;

  form = new FormGroup({
    categ: new FormControl("", Validators.required)
  });

  constructor(private modalService: NgbModal) {}

  ngOnInit() {
    this.admin = JSON.parse(localStorage.getItem("user")).user.role;
  }

  handleDeleteCategory(category) {
    this.deleteCategory.emit(category);
  }

  open(content, category) {
    this.categoryToEdit = category;
    this.modalRef = this.modalService.open(content);
  }

  edit() {
    this.categoryToEdit.name = this.form.value.categ;
    this.editCategory.emit(this.categoryToEdit);
    this.modalRef.close();
  }

  handleEditCategory(category) {
    this.editCategory.emit(category);
  }
}
