import { PRODUCTS } from './../../../../config';
import { Component, OnInit } from '@angular/core';
import { CoreService } from '../../../../core.service';
import swal from 'sweetalert2';

@Component({
  selector: 'dash-products',
  templateUrl: './dash-products.component.html',
  styleUrls: ['./dash-products.component.css']
})
export class DashProductsComponent implements OnInit {

  products: any;

  constructor(private service: CoreService) { }

  ngOnInit() {
    this.service.getProducts()
      .subscribe(products => {
        this.products = products;
      });
  }

  handleDeleteProduct(product) {
    swal({
      title: 'Are you sure?',
      text: 'Delete Product: ' + product.name,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then(result => {
      if (result.value) {
        this.service.deleteProduct(product)
          .subscribe(res => {
            let i = this.products.indexOf(product);
            this.products.splice(i, 1);
            swal('Deleted!', 'Product Deleted', 'success');
          });
      }
    });
  }
}
