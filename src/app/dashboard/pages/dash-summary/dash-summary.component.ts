import { PRODUCTS, CATEGORY } from './../../../config';
import { CoreService } from './../../../core.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'dash-summary',
  templateUrl: './dash-summary.component.html',
  styleUrls: ['./dash-summary.component.css']
})
export class DashSummaryComponent implements OnInit {

  products;
  categories;

  constructor(private service: CoreService) { }

  ngOnInit() {
    this.service.getProductsOfUser()
      .subscribe(products => this.products = products);

    this.service.getCategory()
      .subscribe(categories => this.categories = categories);
  }
}
