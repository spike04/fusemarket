import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {

  @Output() addCategory = new EventEmitter();

  form = new FormGroup({
    name: new FormControl('', Validators.required)
  });

  get name() {
    return this.form.get('name');
  }

  constructor() { }

  ngOnInit() {
  }

  handleAddCategory() {
    this.addCategory.emit(this.form.value.name);
    this.form.reset();
  }

}
