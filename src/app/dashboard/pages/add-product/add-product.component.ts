import { CATEGORY } from './../../../config';
import { CoreService } from './../../../core.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  categories;
  imageFile;
  bannerImage = '';

  form = new FormGroup({
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    category: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    negotiable: new FormControl('yes'),
    contact: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required)
  });

  get name() {
    return this.form.get('name');
  }
  get description() {
    return this.form.get('description');
  }
  get category() {
    return this.form.get('category');
  }
  get price() {
    return this.form.get('price');
  }
  get negotiable() {
    return this.form.get('negotiable');
  }
  get contact() {
    return this.form.get('contact');
  }
  get address() {
    return this.form.get('address');
  }

  printCategory() {
    console.log('CATEGORY', this.form.value.negotiable);
  }

  constructor(private service: CoreService, private router: Router) { }

  ngOnInit() {
    this.service.getCategory()
      .subscribe(categories => this.categories = categories);
  }

  imageLoaded(event) {
    if (event.target.files && event.target.files[0]) {
      this.imageFile = event.target.files[0];
      let reader = new FileReader();
      reader.onload = (e: any) => {
        this.bannerImage = e.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  addProduct() {
    let formData = new FormData();
    formData.append('name', this.form.value.name);
    formData.append('description', this.form.value.description);
    formData.append('price', this.form.value.price);
    formData.append('negotiable', this.form.value.negotiable);
    formData.append('contact', this.form.value.contact);
    formData.append('address', this.form.value.address);
    formData.append('file', this.imageFile);
    formData.append('category', this.form.value.category);
    formData.append('userId', JSON.parse(localStorage.getItem('user')).user._id);

    console.log('FORM DATA ADD', formData);

    this.service.addProduct(formData)
      .subscribe(res => {
        this.router.navigate(['/dashboard/product']);
      });
  }
}
