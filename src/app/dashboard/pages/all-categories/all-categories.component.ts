import { Component, OnInit } from "@angular/core";
import swal from "sweetalert2";

import { CoreService } from "./../../../core.service";

@Component({
  selector: "all-categories",
  templateUrl: "./all-categories.component.html",
  styleUrls: ["./all-categories.component.css"]
})
export class AllCategoriesComponent implements OnInit {
  categories;

  categoryToEdit;
  showModal;

  admin;

  constructor(private service: CoreService) {}

  ngOnInit() {
    this.admin = JSON.parse(localStorage.getItem("user")).user.role;
    this.service
      .getCategory()
      .subscribe(categories => (this.categories = categories));
  }

  editCategory(category) {
    this.categoryToEdit = category;
    this.service.updateCategory(category).subscribe();
  }

  deleteCategory(category) {
    swal({
      title: "Are you sure?",
      text: "Delete Category: " + category.name,
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, keep it"
    }).then(result => {
      if (result.value) {
        this.service.deleteCategory(category).subscribe(res => {
          let i = this.categories.indexOf(category);
          this.categories.splice(i, 1);
          swal("Deleted!", "Category Deleted", "success");
        });
      }
    });
  }

  addCategory(name) {
    this.service.addCategory(name).subscribe((res: any) => {
      this.categories.push(res.category);
      swal("Great", "Category Added ...", "success");
    });
  }

  close() {
    this.showModal = false;
  }
}
