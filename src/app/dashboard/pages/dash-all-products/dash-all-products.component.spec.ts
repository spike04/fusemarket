import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashAllProductsComponent } from './dash-all-products.component';

describe('DashAllProductsComponent', () => {
  let component: DashAllProductsComponent;
  let fixture: ComponentFixture<DashAllProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashAllProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashAllProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
