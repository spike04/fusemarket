import { CATEGORY } from './../../../config';
import { CoreService } from './../../../core.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  product: any;

  categories: any;

  bannerImage;

  changed = false;

  imageFile;

  form = new FormGroup({
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    category: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    negotiable: new FormControl('yes'),
    contact: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required)
  });

  get name() {
    return this.form.get('name');
  }
  get description() {
    return this.form.get('description');
  }
  get category() {
    return this.form.get('category');
  }
  get price() {
    return this.form.get('price');
  }
  get negotiable() {
    return this.form.get('negotiable');
  }
  get contact() {
    return this.form.get('contact');
  }
  get address() {
    return this.form.get('address');
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private service: CoreService,
    private router: Router) { }

  ngOnInit() {
    this.service.getCategory()
      .subscribe(categories => {
        this.categories = categories;
        this.activatedRoute.params.subscribe(value => {
          this.service.getProduct(value.id)
            .subscribe((product: any) => {
              this.product = product;
              this.form.patchValue({
                name: product.name,
                description: product.description,
                category: product.category._id,
                price: product.price,
                negotiable: product.negotiable,
                contact: product.contact,
                address: product.address
              })

              this.bannerImage = 'http://localhost:5100/uploads/' + product.productImage + '/200';
            });
        })
      });
  }

  imageLoaded(event) {
    if (event.target.files && event.target.files[0]) {
      this.imageFile = event.target.files[0];
      let reader = new FileReader();
      reader.onload = (e: any) => {
        this.changed = true;
        this.bannerImage = e.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  editProduct() {
    let formData = new FormData();
    formData.append('name', this.form.value.name);
    formData.append('description', this.form.value.description);
    formData.append('price', this.form.value.price);
    formData.append('negotiable', this.form.value.negotiable);
    formData.append('contact', this.form.value.contact);
    formData.append('address', this.form.value.address);
    if (this.changed) {
      formData.append('file', this.imageFile);
    }
    formData.append('changed', String(this.changed));
    console.log('CHANGED', this.changed);
    formData.append('category', this.form.value.category);
    formData.append('userId', JSON.parse(localStorage.getItem('user')).user._id);

    console.log('FORMDATA HOME', formData);

    this.service.editProduct(this.product._id, formData)
      .subscribe(res => {
        this.router.navigate(['/dashboard/product']);
      });
  }
}
