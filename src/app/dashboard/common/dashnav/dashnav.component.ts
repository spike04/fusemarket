import { AuthService } from './../../../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'dashnav',
  templateUrl: './dashnav.component.html',
  styleUrls: ['./dashnav.component.css']
})
export class DashnavComponent implements OnInit {

  constructor(private service: AuthService, private router: Router) { }

  ngOnInit() {
  }

  logout() {
    this.service.logout();
    this.router.navigate(['/']);
  }

}
