import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MomentModule } from 'angular2-moment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthService } from './auth.service';
import { BsnavComponent } from './common/bsnav/bsnav.component';
import { SidebarComponent } from './common/sidebar/sidebar.component';
import { CoreService } from './core.service';
import { DashnavComponent } from './dashboard/common/dashnav/dashnav.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddCategoryComponent } from './dashboard/pages/add-category/add-category.component';
import { AddProductComponent } from './dashboard/pages/add-product/add-product.component';
import { AllCategoriesComponent } from './dashboard/pages/all-categories/all-categories.component';
import { DashCategoriesComponent } from './dashboard/pages/components/dash-categories/dash-categories.component';
import { DashProductsComponent } from './dashboard/pages/components/dash-products/dash-products.component';
import { DashAllProductsComponent } from './dashboard/pages/dash-all-products/dash-all-products.component';
import { DashSummaryComponent } from './dashboard/pages/dash-summary/dash-summary.component';
import { AllProductsComponent } from './home/all-products/all-products.component';
import { CategoryProductComponent } from './home/category-product/category-product.component';
import { FeaturedProductsComponent } from './home/featured-products/featured-products.component';
import { MainComponent } from './home/main/main.component';
import { ProductDetailComponent } from './home/product-detail/product-detail.component';
import { ProductItemComponent } from './home/product-item/product-item.component';
import { SearchProductComponent } from './home/search-product/search-product.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EditProductComponent } from './dashboard/pages/edit-product/edit-product.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    MainComponent,
    FeaturedProductsComponent,
    AllProductsComponent,
    BsnavComponent,
    SidebarComponent,
    LoginComponent,
    RegisterComponent,
    DashnavComponent,
    DashSummaryComponent,
    AddProductComponent,
    AllCategoriesComponent,
    AddCategoryComponent,
    DashAllProductsComponent,
    DashProductsComponent,
    DashCategoriesComponent,
    ProductItemComponent,
    CategoryProductComponent,
    SearchProductComponent,
    ProductDetailComponent,
    EditProductComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MomentModule,
    AppRoutingModule,
    NgbModule.forRoot()
  ],
  providers: [CoreService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
